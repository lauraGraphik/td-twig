<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FrontController extends AbstractController
{
    /**
     * @Route("/", name="front")
     */
    public function index(): Response
    {
        return $this->render('front/index.html.twig');
    }
    /**
     * @Route("/saison/{saison}", name="front_saison")
     */
    public function saison(string $saison): Response
    {
        if($saison == "été") $saison=="ete";
        if(in_array($saison, ["automne", "ete", "hiver", "printemps"] )){
            return $this->render('front/saison.html.twig', ["saison"=>$saison]);
        }else{
            return $this->render("front/404.html.twig");
        }
    }
}
